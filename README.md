## api-encuesta

- Esta api tiene como finalidad, ingresar y obtener los resultados de la encuesta.

## Requisitos y detalles de los sistemas

- La maquina en donde se vaya a levantar la api, debe contar con maven 3 y java 8 instalados.
- Version de SpringBoot utilizado 2.4.5
- Para ejecutar api-encuesta, debe estar liberado el puerto 8080


## Compilar y ejecutar los proyectos

- Realizar pasos en el orden descrito
- Para ejecutar api-encuesta

    1) Dirigirse al directorio EncuestaFront/api-encuaesta
    2) Una vez dentro, ejecutar el siguiente comando
       ----
       mvn clean install
       ----
    3) Luego ejecutar el siguiente comando
       ----
       java -jar target/api-encuesta-0.0.1-SNAPSHOT.jar
       ----
       o en su defecto
       ----
       mvn spring-boot:run
       ----
  (recordar tener disponible el puerto 8080)