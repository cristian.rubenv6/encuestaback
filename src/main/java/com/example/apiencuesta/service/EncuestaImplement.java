package com.example.apiencuesta.service;
import com.example.apiencuesta.model.Encuesta;
import com.example.apiencuesta.repository.EncuestaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EncuestaImplement implements IEncuesta{

    @Autowired
    private EncuestaRepository repository;

    @Override
    @Transactional
    public Encuesta insert(Encuesta encuesta) {
        return repository.save(encuesta);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Encuesta> getAll() {
        return (List<Encuesta>)repository.findAll();
    }
}
