package com.example.apiencuesta.service;

import com.example.apiencuesta.model.Encuesta;

import java.util.List;

public interface IEncuesta {

    public Encuesta insert(Encuesta encuesta);

    public List<Encuesta> getAll();
}
