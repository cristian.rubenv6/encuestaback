package com.example.apiencuesta.controller;

import com.example.apiencuesta.model.Encuesta;
import com.example.apiencuesta.service.IEncuesta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class EncuestaController {

    @Autowired
    private IEncuesta encuestaService;

    @PostMapping("/ingresar-respuesta")
    public ResponseEntity add(@RequestBody Encuesta encuesta){

        try{
            return new ResponseEntity<>(encuestaService.insert(encuesta), HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping("/ver-todos")
    public ResponseEntity getAll(){
        try{
            return new ResponseEntity<>(encuestaService.getAll(), HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
