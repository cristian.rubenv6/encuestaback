package com.example.apiencuesta.repository;

import com.example.apiencuesta.model.Encuesta;
import org.springframework.data.repository.CrudRepository;

public interface EncuestaRepository extends CrudRepository<Encuesta, String> {
}
