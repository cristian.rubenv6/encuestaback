package com.example.apiencuesta.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="encuesta")
public class Encuesta implements Serializable {

    private static final long serialVersionUID = 5656006406841434495L;

    @Id
    private String email;

    @Column(name="estilosmusicales")
    private String estilosMusicales;

    public Encuesta(String email, String estilosMusicales) {
        this.email = email;
        this.estilosMusicales = estilosMusicales;
    }

    public Encuesta() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEstilosMusicales() {
        return estilosMusicales;
    }

    public void setEstilosMusicales(String estilosMusicales) {
        this.estilosMusicales = estilosMusicales;
    }
}
