package com.example.apiencuesta;

import com.example.apiencuesta.model.Encuesta;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ApiEncuestaApplicationTests {

    @Test
    void testEncuestaEmail() {
        Encuesta enc = new Encuesta();
        enc.setEmail("cristian@gmail.com");
        Assertions.assertEquals("cristian@gmail.com", enc.getEmail());
    }

    @Test
    void testEncuestaEstilosMusicales(){
        Encuesta enc = new Encuesta();
        enc.setEstilosMusicales("Rock,Metal,Reggae");
        Assertions.assertEquals("Rock,Metal,Reggae", enc.getEstilosMusicales());
    }

    @Test
    void testReferenciaEncuesta() {
        Encuesta encuesta = new Encuesta("cristian@gmail.com", "Rock,Metal,Reggae");
        Encuesta encuesta2 = new Encuesta("cristian@gmail.com", "Rock,Metal,Reggae");
        Assertions.assertNotEquals(encuesta2, encuesta);
    }
}
